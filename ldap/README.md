# LDAP operation against FreeIPA
A collection of commands meant for testing ldap service hosted by a FreeIPA server

## Assumptions
* Target freeipa server is operational
* FreeIPA server is a remote host
* Client host has installed ldapsearch

## Systemprep
```bash
# Clone the repository
git clone git@github.com:jesmigel/freeipa-demo.git

# CD to ldap test directory
cd freeipa-demo/operations/ldap

# Update env vars
vi common.env
```

## LDAP tests
```bash
# Poll LDAP
make poll

# Test ldap authentication
make auth
```