.PHONY: setpass print_install_server print_install_client

include common.env
include ipa.conf.env

setpass:
	openssl rand -base64 32 > sample.ipapass

server:
	$(call set_tz,$(_COMMON_TIMEZONE))
	$(call update_pkgmgr,yum)
	$(call install_os_dependencies,server,yum)
	$(call set_hostname,$(_SERVER_FQDN))
	$(call install_ipa,server,$(_SERVER_IPA))

client:
	$(call set_tz,$(_COMMON_TIMEZONE))
	$(call install_os_dependencies,client,apt)
	$(call set_hostname,$(_CLIENT_FQDN))
	$(call install_ipa,client,$(_CLIENT_IPA))

cert:
	kinit $(SERVICE_ACCOUNT)
	sudo ipa-getcert request -r \
	-f /etc/pki/ca.crt \
	-k /etc/pki/ca.key \
	-N CN=`hostname -f` \
	-D `hostname -f` -K HTTP/`hostname -f`



# Install ipa
# 1: Component {client,server}
# 2: Configuration
define install_ipa
	@echo "# Install FreeIPA $(1)"
	@echo "sudo bash -c 'ipa-$(1)-install $(2)'"
	@echo ""
endef

# Set Hostname
# 1: FQDN
define set_hostname
	@echo "# Set Hostname"
	@echo "sudo hostnamectl set-hostname $(1) --static"
	@echo ""
endef

# Install dependencies
# 1: Component {client,server}
# 2: Package manager {apt,yum}
define install_os_dependencies
	@echo "Installing $(1) $(2) dependencies"
	@echo "sudo bash -c '$(2) install -y $$(cat ipa.$(1).$(2).txt)'"
	@echo ""
endef

# Update/Upgrade OS
# 1: Package manager {apt,yum}
define update_pkgmgr
	@echo "Updating and upgrading package manager: $(1)"
	@echo "sudo bash -c '$(1) update -y && $(1) upgrade -y'"
	@echo ""
endef

# Set TimeZone
# 1: TimeZone
define set_tz
	@echo "# Set Timezone: $(1)"
	@echo "sudo bash -c 'timedatectl set-timezone \"$(1)\"'"
	@echo ""
endef
