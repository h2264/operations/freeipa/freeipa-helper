# freeipa-helper
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Installation guide for a homelab install of FreeIPA

## Assumptions
* Server Host is [Fedora](https://getfedora.org/en/server/download/) release 34 Server edition.
    * [Checksum](https://getfedora.org/en/security/) ideally should be validated per release.
* Server Host resources are as follows:
    * CPU: 4
    * RAM: 4GB
    * Storage: 32GB
* Client Host is an Ubuntu OS with [Focal Fossa (20.04)](https://cloud-images.ubuntu.com/focal/) or [Hirsuite (21.04)](https://cloud-images.ubuntu.com/hirsute/) release
* Target host has access to the internet


## System prep
```bash
# Switch to the root account
sudo su -

# Generate Base64 formatted password
make setpass
```

## Sample Install Freeipa Server
```bash
# Print demo freeipa-server install (Fedora Host)
make server
# Set Timezone: Australia/Melbourne
sudo bash -c 'timedatectl set-timezone "Australia/Melbourne"'

# Updating and upgrading package manager: yum
sudo bash -c 'yum update -y && yum upgrade -y'

# Installing server yum dependencies
sudo bash -c 'yum install -y samba \
freeipa-server \
ipa-server-dns \
bind-dyndb-ldap'

# Set Hostname
sudo hostnamectl set-hostname ipa-0.fwc.dc.ops.vm --static
sudo hostnamectl set-hostname ipa-1.fwc.dc.ops.vm --static

# Install FreeIPA server
export PASSWORD=$SET_PASSWORD_HERE
sudo ipa-server-install \
--auto-reverse \
--setup-dns \
--forwarder=8.8.8.8 \
--forwarder=1.1.1.1 \
--reverse-zone=1.1.10.in-addr.arpa. \
--ntp-pool=pool.ntp.org \
--hostname=`hostname -f` \
--domain=fwc.dc.ops.vm \
--realm=FWC.DC.OPS.VM \
--ssh-trust-dns \
--ds-password=$PASSWORD \
--admin-password=$PASSWORD \
--unattended

# Replication setup
sudo bash -c 'yum install -y samba \
freeipa-server \
freeipa-client \
ipa-server-dns \
bind-dyndb-ldap'

sudo ipa-client-install \
--principal=admin \
--ntp-pool=pool.ntp.org \
--domain=fwc.dc.ops.vm \
--realm=FWC.DC.OPS.VM \
--server=ipa-0.fwc.dc.ops.vm \
--all-ip-addresses \
--hostname=`hostname -f` \
--mkhomedir \
--password=$PASSWORD \
--unattended

kinit admin 
sudo ipa dnsrecord-add fwc.dc.ops.vm ipa-1 --a-ip-address 10.1.1.57

sudo ipa-replica-install \
--principal=admin \
--admin-password=$PASSWORD \
--mkhomedir \
--unattended
```

## FreeIPA post installation notes
- Enable DNS recursion by adding `allow-recursion { any; };` inside `options {}` in the file `/etc/named.conf`
```bash
# Print conf file
cat /etc/named.conf
/* WARNING: This config file is managed by IPA.
 *
 * DO NOT MODIFY! Any modification will be overwritten by upgrades.
 *
 *
 * - /etc/named/ipa-options-ext.conf (for options)
 * - /etc/named/ipa-logging-ext.conf (for logging options)
 * - /etc/named/ipa-ext.conf (all other settings)
 */

options {
        allow-recursion { any; };
...
```
## Sample Install FreeIPA Client
```bash
# Print demo freeipa-client install (Ubuntu Host)
make client
# Set Timezone: Australia/Melbourne
sudo bash -c 'timedatectl set-timezone "Australia/Melbourne"'

# Installing client apt dependencies
sudo bash -c 'apt install -y freeipa-client'

# Set Hostname
sudo hostnamectl set-hostname `hostname -f`.fwc.dc.ops.vm --static

# Install FreeIPA client
sudo ipa-client-install \
--principal=admin \
--ntp-pool=pool.ntp.org \
--domain=fwc.dc.ops.vm \
--realm=FWC.DC.OPS.VM \
--server=ipa-0.fwc.dc.ops.vm \
--all-ip-addresses \
--hostname=`hostname -f` \
--mkhomedir \
--password=$PASSWORD \
--unattended

```

# FreeIPA client certificate request
```bash
# Kerberos authentication
kinit $SERVICE_ACCOUNT

# Request certificate
sudo ipa-getcert request -r \
-f /etc/pki/`hostname -f`.crt \
-k /etc/pki/`hostname -f`.key \
-N CN=`hostname -f` \
-D `hostname -f` -K HTTP/`hostname -f`

# Display certificate
sudo ipa-getcert list
```

## Tests
| Label | Comment |
| --- | --- |
|[ldap](./operations/ldap/README.md)| Authentication service |
